#!/usr/bin/env bash

# Variaveis
dia_hora_atual=$( date +'%d/%m/%Y - %H:%M' )
editor="vim"
header="#!/usr/bin/env bash
# -------------------------------------------------------------------------------------------------
# Script	:
# Descrição	:
# Versão	: 0.1.0
# Autor		: Wandiêr Tomé <wad.tome@gmail.com>
# Data/Hora	: $dia_hora_atual
# Licença	:
# -------------------------------------------------------------------------------------------------
# Uso	:
# -------------------------------------------------------------------------------------------------
"

[[ $# -ne 1 ]] && echo "É preciso de um nome para o arquivo" && exit 1

[[ -f $1 ]] &&  echo "O arquivo já existe! Digite outro 'Nome'" && exit 1

echo "$header" > $1
chmod +x $1
$editor $1

exit 0
