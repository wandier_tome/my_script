#!/usr/bin/env bash
IDESK=$(xdotool get_desktop)
NDESK=$(xdotool get_num_desktops)

desk_prev(){
	[[ $(( $IDESK - 1 )) -lt 0 ]] && prev=$(( $NDESK - 1 )) || prev=$(( $IDESK - 1 ))
	xdotool set_desktop $prev
	xfce4-panel --plugin-event=genmon-2:refresh:bool:true
}

desk_next(){
	xdotool set_desktop --relative 1
	xfce4-panel --plugin-event=genmon-2:refresh:bool:true
}

desk_current(){
	echo "<txt>$(($IDESK + 1))</txt>"
	echo "<txtclick>xdotool key ctrl+alt+d</txtclick>"
}

case $1 in
	"next") desk_next
		;;
	"prev") desk_prev
		;;
	"show") desk_current
		;;
esac
